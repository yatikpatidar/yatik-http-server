const fs = require('fs')
const http = require("http");
const uuid = require('uuid')

const hostname = '127.0.0.1';
const port = 3000;

const server = http.createServer((req, res) => {

    try {
        if (req.method === 'GET' && req.url === '/html') {
            fs.readFile('./index.html', { encoding: 'utf8' }, (err, data) => {
                if (err) {
                    res.end(err.message)
                }
                res.end(data)
            })
        }

        if (req.method === 'GET' && req.url === '/json') {
            fs.readFile('./data.json', { encoding: 'utf8' }, (err, data) => {

                if (err) {
                    res.end(err.message)
                }
                res.writeHead(200, { 'Content-Type': 'text/plain' })
                res.end(data)
            })
        }

        if (req.method === 'GET' && req.url === '/uuid') {
            const obj = {
                "uuid": uuid.v4()
            }
            res.writeHead(200, { 'Content-Type': 'text/plain' })
            res.end(JSON.stringify(obj))
        }

        const inputUrl = req.url.split("/")

        if (req.method === 'GET' && inputUrl[1] === 'status') {
            res.writeHead(200, { 'Content-Type': 'text/plain' })
            res.write(http.STATUS_CODES[inputUrl[2]])
            res.end()
        }

        if (req.method === 'GET' && inputUrl[1] === 'delay') {
            const delay = inputUrl[2]
            setTimeout(() => {
                res.writeHead(200, { 'Content-Type': 'text/plain' })
                res.write(http.STATUS_CODES[200])
                res.end()

            }, delay * 1000)
        }
    } catch (err) {
        res.end(err.message)
    }
})

server.listen(port, hostname, () => {
    console.log("server started at local host:3000")
})